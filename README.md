# update-gitea-ctimes

A script to modify creation time flags in Gitea to match the oldest Git commit.

This is helpful for ordering repositories chronologically by creation-time after a migration to Gitea (e.g. for [teafolio](https://code.ivysaur.me/teafolio)).

Written in PHP

## Usage

```
Usage: update-gitea-ctimes gitea.db path/to/repositories
```

Running the command will produce SQL on stdout that can be manually inspected before applying it to the live SQLite database.
